var c, ctx;
var ballonBleu = new Image();
var ballonRouge = new Image();
var ballonJaune = new Image();
var explosion = new Image();
var fps, posX, posY, vitesseY;
var WIDTH = 30;
var HEIGHT = 40 ;
var isHit;
var score ;

function init()
{
	c = document.getElementById('game');
	c.onmousemove = function(e){
		displayPosCursor(e,this);
	}
	
	ctx = c.getContext("2d");
	fps = 30;
	posX = Math.floor((Math.random() * Math.floor(c.width - 50)) + 1);
	isHit = false;
	posY = 100;
	vitesseY = -1;
	ballonBleu.src = "image/ballonBleu.png";
	ballonRouge.src = "image/ballonRouge.png";
	ballonJaune.src = "image/ballonJaune.png";
	explosion.src = "image/explosion.png"
	score = 0;
	window.requestAnimationFrame(draw);
}


function draw()
{
  
  setTimeout(function() 
  {
	
	requestAnimationFrame(draw);
	ctx.globalCompositeOperation = 'destination-over';
	drawBallonBleu();
	drawBallonJaune();
	
  }, 1000 / fps);
  
}

function DrawingElement(image, yPos, xPos)
{
	ctx.clearRect(0,0,c.width,c.height);
	ctx.save();
	ctx.translate(0,vitesseY);
	ctx.drawImage(image,xPos,yPos,WIDTH,HEIGHT);
	ctx.fillStyle = "rgba(200,200,200,0.0)";
    ctx.fillRect(xPos, yPos, WIDTH, HEIGHT);
	ctx.restore();
}

function drawBallonBleu()
{
	if(isHit == false)
	{
		DrawingElement(ballonBleu, posY, posX);
		posY  += vitesseY ;
		if(posY == 0){
			DrawingElement(ballonBleu, posY, posX);
			posY = 100;
			posX = Math.floor((Math.random() * Math.floor(c.width - 50)) + 1);
		}
		c.onclick = function(e){
			isHit = shoot(e,this);
			if(isHit){ 
				updateScore();
				
			}
		}
		displayPosBallon(posX, posY);
	}
	else if(isHit = true)
	{
		DrawingElement(explosion, posY, posX);
		setTimeout(function(){ 
						isHit = false ; 
						posY = 100;
						posX = Math.floor((Math.random() * Math.floor(c.width - 50)) + 1);
						drawBallonBleu(); 
				   },1000);
		
		
	}
}

function drawBallonJaune()
{
	if(isHit == false)
	{
		
		DrawingElement(ballonJaune, posY, posX);
		posY  += vitesseY ;
		if(posY == 0){
			DrawingElement(ballonJaune, posY, posX);
			posY = 100;
			posX = Math.floor((Math.random() * Math.floor(c.width - 50)) + 1);
		}
		c.onclick = function(e){
			isHit = shoot(e,this);
		}
		displayPosBallon(posX, posY);
	}
	else if(isHit = true)
	{
		DrawingElement(explosion, posY, posX);
		updateScore();
		//isHit = false ;
	}
}

function displayPosBallon(X,Y)
{
	document.getElementById('posX').innerHTML = X;
	document.getElementById('posY').innerHTML = Y;
	
	
}

function displayPosCursor(event,el)
{
	
	
	var ox = -el.offsetLeft, oy = -el.offsetTop;
	var Mouse_X, Mouse_Y; 
	while(el=el.offsetParent){
		ox += el.scrollLeft - el.offsetLeft;
		oy += el.scrollTop - el.offsetTop;
		
	}
	Mouse_X = event.clientX; 
	Mouse_Y = event.clientY; 
	document.getElementById('X').innerHTML = Math.floor((Mouse_X + ox) / 3.14); //ratio en x positionSouris / positionBallon
	document.getElementById('Y').innerHTML = Math.floor((Mouse_Y + oy) / 2.65) ; //ratio en y positionSouris / positionBallon
}


function shoot(event,el)
{
	
	var ox = -el.offsetLeft, oy = -el.offsetTop;
	var Mouse_X, Mouse_Y; 
	while(el=el.offsetParent){
		ox += el.scrollLeft - el.offsetLeft;
		oy += el.scrollTop - el.offsetTop;
		
	}
	Mouse_X = Math.floor((event.clientX + ox) / 3.14); 
	Mouse_Y = Math.floor((event.clientY + oy) / 2.65); 
	if((Mouse_X > posX && Mouse_X < posX + WIDTH) && (Mouse_Y > posY && Mouse_Y < posY + HEIGHT))
	{
		return true;
	}
	else
	{
		return false;
	}




}


function updateScore(){

	score += 1;
	document.getElementById('nombrePoint').innerHTML = score;

}
init();






